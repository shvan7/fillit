#include "fillit.h"

// RECURCIVE
int ft_replace_tetrix(t_map *data, int index, int max)
{
  // si on arrive a placer le dernier element
  if (index == data->column)
    return (1);

  // bloucle tant que tab depasse pas le max size
  while (data->tab[index][3] < max)
  {
    // si pas de chevauchement rappel recursive
    if (!(ft_overlap(data, index)))
      if (ft_replace_tetrix(data, index + 1, max))
        return (1);
    // avance le tetris
    ft_upgrade(data, index);
  }
  // return origin
  ft_index_origin(data, index);
  return (0);
}

// APPEL RECURSIVE (au dessus)
int ft_run_process(t_map *data)
{
  int i;
  int new_size;

  i = -1;
  new_size = ft_sqrt(data->column * 4);
  printf("new size : %d\n", new_size);
  while (++i < data->column)
    ft_update_index(data->tab[i], data->size, new_size);
  data->size = new_size;

  // SUITE
  while (!(ft_replace_tetrix(data, 0, data->size * data->size)))
    ft_reset_grown(data);

  return (1);
}

// GESTIONNAIRE ERREUR ETC
int ft_core(int fd)
{
  char *file;
  t_map *data;
  int column = 4;

  if (!(file = ft_reader_file(fd)))
    return (-1);
  if ((column = ft_check_file(file)) < 0)
    return (-2);
  if (!(data = ft_parse_tetrix(file, column)))
    return (-3);
  if (ft_check_tetrix(data) < 0)
    return (-4);

  free(file);
  file = NULL;

  ft_display_somme(data);
  ft_run_process(data);
  ft_display_tab(data);
  return (0);
}