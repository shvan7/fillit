/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 15:22:57 by aulima-f          #+#    #+#             */
/*   Updated: 2019/01/28 15:23:25 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H

# define FILLIT_H
# define MAX_CHAR 1000

# include <unistd.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdio.h>
# include "libft.h"

typedef struct s_map
{
  int **tab;
  int column;
  int size;
} t_map;

// parser
t_map *ft_build_struct(int column);
t_map *ft_parse_tetrix(char *file, int column);
char *ft_reader_file(int fd);

// checker
int ft_check_file(char *file);
int ft_check_tetrix(t_map *data);

// process
int ft_replace_tetrix(t_map *data, int index, int max);
int ft_run_process(t_map *data);
int ft_core(int fd);

// process (utilis)
int ft_find_int(t_map *data, int value, int i);
int ft_overlap(t_map *data, int i);
void ft_update_index(int *tab, int size, int new_size);
void ft_reset_grown(t_map *data);

// generic
int ft_iter(int size, int *tab);
void ft_upgrade(t_map *data, int index);
void ft_index_origin(t_map *data, int index);

// display
int ft_get_index_display(t_map *data, int value);
void ft_display_tab(t_map *data);
void ft_display(t_map *data, int index);
void  ft_display_one(t_map *data, int index);
void ft_display_somme(t_map *data);

#endif
