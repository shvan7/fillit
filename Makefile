# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/12 10:45:11 by aulima-f          #+#    #+#              #
#    Updated: 2018/11/23 19:47:46 by aulima-f         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

################################ STYLE ############################################
GREEN = \033[0;32m
WHITE = \033[1;37m
BLUE = \033[1;34m
RED = \033[0;31m
YELLOW = \033[1;33m

OKGREEN = $(YELLOW)\t===== $(GREEN)[OK]$(WHITE)
KORED = $(YELLOW)\t===== $(RED)[error]$(WHITE)

#*********************************************************************************
#---------------------------------------------------------------------------------
CC = gcc
AR = ar rc
RAN = ranlib
FLAGS = -Wall -Wextra -Werror
NAME = fillit

SRC = parser.c  \
			 checker.c \
			 process.c \
			 process_utilis.c \
			 generic.c \
			 display.c \
			 main.c

HEADERS = fillit.h
HEADERS_LIB = libft/
LIB = libft/libft.a
OBJECT = $(SRC:.c=.o)

all: $(NAME)

%.o: %.c
	@$(CC) $(FLAGS) -o $@ -c $< -I$(HEADERS) -I$(HEADERS_LIB)

$(LIB):
	make -C ./libft/

$(NAME): $(OBJECT) $(LIB) Makefile
	@$(CC) $(FLAGS) -o $@ $(OBJECT) -Llibft -lft && echo "- $(BLUE)Create prog$(OKGREEN)" || (echo "- $(BLUE)Create prog$(KORED)" && false)

$(DIR_LIB):
	@$(MAKE) -C libft/

clean:
	@rm -rf $(OBJECT)
	@echo "- $(BLUE)Delete object$(OKGREEN)"

cclean: all clean

fclean: clean
	@rm -rf $(NAME)
	@echo "- $(BLUE)Delete lib$(OKGREEN)"

push:
	@git add -A
	@git commit -m 'auto push'
	@git push origin master
	@echo "- $(BLUE)Push ok on$(OKGREEN)"
	@git config remote.origin.url

re: fclean all
