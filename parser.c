/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 15:23:18 by aulima-f          #+#    #+#             */
/*   Updated: 2019/01/28 15:23:20 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "fillit.h"

// construit la liste
t_map *ft_build_struct(int column)
{
  t_map *data;

  if (!(data = (t_map *)ft_memalloc(sizeof(t_map))))
    return (NULL);
  data->tab = ft_tabnew(column, 4);
  data->column = column;
  data->size = 4;
  return (data);
}

// modifie le fichier en enlevant les \n
// sauvegarde les index
t_map *ft_parse_tetrix(char *file, int column)
{
  int i;
  int j;
  int k;
  t_map *data;

  i = 0;
  j = 0;
  k = 0;
  data = ft_build_struct(column);
  ft_remchar(file, '\n', ft_strlen(file));
  while (file[i++])
  {
    if (file[i] == '#')
      data->tab[j][k++] = i - 16 * (i / 16);
    if (k == 4)
    {
      k = 0;
      j++;
    }
  }
  return (data);
}

// read le fichier buff max de 500 (26 => tretrix max)
char *ft_reader_file(int fd)
{
  char *buff;

  if (!(buff = (char *)ft_strnew(MAX_CHAR)))
    return (NULL);
  if (read(fd, buff, MAX_CHAR) < 0)
    return (NULL);
  return (buff);
}