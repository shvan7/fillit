#include "fillit.h"

// verifie le nombre de relation (3) de chaque tetris
// si ok commence le process
int ft_check_tetrix(t_map *data)
{
  int i;

  i = -1;
  while (++i < data->column)
  {
    if (ft_iter(4, data->tab[i]) < 3)
      return (-1);
    ft_index_origin(data, i);
  }
  return (1);
}

// verifie les retour de ligne sur le fichier
// compte le nombre de tetris
int ft_check_file(char *file)
{
  int i;
  int column;

  i = -1;
  column = 0;
  if ((ft_strlen(file) + 1) % 21 != 0)
    return (-1);
  file[ft_strlen(file)] = '\n';
  while (file[++i])
  {
    if ((i + 1) % 5 == 0 && file[i] == '\n')
      i++;
    if ((i + 1) % 21 == 0 && file[i] == '\n')
    {
      file += i + 1;
      i = 0;
      column++;
    }
    if (file[i] != '.' && file[i] != '#' && file[i] != '\0')
      return (-1);
  }
  return (column);
}