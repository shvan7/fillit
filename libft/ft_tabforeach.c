
#include "libft.h"
#include <string.h>

void ft_tabforeach(int *tab, size_t n, int (*f)(int))
{
  while (n--)
    tab[n] = (*f)(tab[n]);
}