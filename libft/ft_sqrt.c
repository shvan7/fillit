#include "libft.h"
#include "stdio.h"

int ft_sqrt(int c)
{
  int nbr;

  nbr = ft_power(10, ft_countdec(c) / 2) + 1;
  while (--nbr > 1)
    if (c >= (nbr * nbr))
      return (nbr);
  return (0);
}