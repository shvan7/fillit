#include "libft.h"
#include <string.h>

int **ft_tabnew(int column, int nb)
{
  int **tab;

  tab = (int **)malloc(sizeof(int *) * column);
  while (column--)
    tab[column] = (int *)malloc(sizeof(int) * nb);
  return (tab);
}