
#include "libft.h"
#include <string.h>

int ft_int_exist(int value, int *tab, size_t n)
{
  while (n--)
    if (tab[n] == value)
      return (1);
  return (0);
}