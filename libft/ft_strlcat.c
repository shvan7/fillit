/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 02:54:23 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:39:04 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

size_t		ft_strlcat(char *dest, const char *src, size_t n)
{
	size_t srclen;
	size_t destlen;

	srclen = ft_strlen(src);
	destlen = ft_strlen(dest);
	destlen = destlen >= n ? n : destlen;
	if (destlen == n)
		return (n + srclen);
	ft_strncat(dest, src, n - destlen - 1);
	return (destlen + srclen);
}
