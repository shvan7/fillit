#include "fillit.h"

int ft_get_index_display(t_map *data, int value)
{
  int i;
  int j;

  i = -1;
  while (++i < data->column)
  {
    j = -1;
    while (++j < 4)
      if (value == data->tab[i][j])
        return (i);
  }
  return (-1);
}

void ft_display_tab(t_map *data)
{
  int i;
  int t;
  int max;

  i = -1;
  t = -1;
  max = data->size * data->size;

  printf("NB BLOC : %d \n", data->column);
  printf("NB SIZE : %d \n", data->size);

  while (++i < max)
  {
    if ((t = ft_get_index_display(data, i)) > -1)
      ft_putchar('A' + t);
    else
      ft_putchar('.');
    if (i != 0 && (i + 1) % (data->size) == 0)
      ft_putchar('\n');
  }
}

void ft_display(t_map *data, int index)
{
  int i;
  int j;

  i = -1;
  j = -1;
  printf("SIZE: %d, INDEX: %d\n", data->size, index);
  while (++i < data->column)
  {
    printf("index : %d -- ", i);
    while (++j < 4)
      printf("(%d) ", data->tab[i][j]);
    printf("\n");
    j = -1;
  }
  printf("\n");
}

void ft_display_one(t_map *data, int index)
{
  int j;

  j = -1;
  while (++j < 4)
    printf("(%d) ", data->tab[index][j]);
  printf("\n");
}

void ft_display_somme(t_map *data)
{
  int i;
  int j;
  int v;

  i = -1;
  v = 0;
  while (++i < data->column)
  {
    j = -1;
    while (++j < 4)
      v += data->tab[i][j];
  }
  printf("{%d}\n", v);
}