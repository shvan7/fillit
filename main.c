#include "fillit.h"
#include <stdio.h>

int main(int ac, char **av)
{
  int fd;
  int res;

  fd = 0;
  if (ac > 1)
    fd = open(av[1], O_RDONLY);
  res = ft_core(fd);
  if (res < 0)
  {
    if (res == -1)
      printf("error : %d lecture fichier \n", res);
    if (res == -2)
      printf("error : %d fichier non conforme \n", res);
    if (res == -3)
      printf("error : %d Structure NULL \n", res);
    if (res == -4)
      printf("error : %d bloc tetris non conforme \n", res);
  } 
  else
    printf("\t\t ==> [OK] : %d Tout fonctionne \n", res);
  return (0);
}