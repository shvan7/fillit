#include "fillit.h"

// return 1 si trouve 1 fois la valeur
int ft_find_int(t_map *data, int value, int i)
{
  int j;

  while (i--)
  {
    j = -1;
    while (++j < 4)
      if (data->tab[i][j] == value)
        return (1);
  }
  return (0);
}

// return 1 si chevauchement
int ft_overlap(t_map *data, int i)
{
  int j;

  j = -1;
  while (++j < 4)
    if (ft_find_int(data, data->tab[i][j], i))
      return (1);
  return (0);
}

// change l'index selon la size
void ft_update_index(int *tab, int size, int new_size)
{
  int i;
  int dif;

  i = -1;
  dif = new_size - size;
  while (++i < 4)
    tab[i] = tab[i] + (tab[i] / size) * dif;
}

// reset le **tab au valeur min et change index si size change
void ft_reset_grown(t_map *data)
{
  int i;

  i = -1;
  while (++i < data->column)
    ft_index_origin(data, i);
  while (i--)
    ft_update_index(data->tab[i], data->size, data->size + 1);
  data->size += 1;
}