#include "fillit.h"

// verifie le nombre de relation en appelant la f au dessus
int ft_iter(int size, int *tab)
{
  int i;
  int count;

  i = -1;
  count = 0;
  while (++i < 3)
  {
    count += ft_int_exist(tab[i] + size, tab, 4);
    if ((tab[i] + 1) % size)
      count += ft_int_exist(tab[i] + 1, tab, 4);
  }
  return (count);
}

void ft_upgrade(t_map *data, int index)
{
  int i;

  i = -1;
  while (++i < 4)
    data->tab[index][i]++;
  if (ft_iter(data->size, data->tab[index]) < 3)
    ft_upgrade(data, index);
}

// enleve -1 a chaque element jusqua ce que tab[0] = -1
// ajoute +1 jusqu'a ce que tab[0] > -1 et que relation > 3 est obtenue
void ft_index_origin(t_map *data, int index)
{
  int i;

  while (data->tab[index][0] > -1)
  {
    i = -1;
    while (++i < 4)
      data->tab[index][i]--;
  }
  ft_upgrade(data, index);
}